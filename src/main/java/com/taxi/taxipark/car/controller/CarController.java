package com.taxi.taxipark.car.controller;

import com.taxi.taxipark.car.entity.CarEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import com.taxi.taxipark.api.CarApi;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by fours on 06.06.2018.
 */
public class CarController {

    private Integer idCounter = 1;
    private List<CarEntity> cars = new ArrayList<>();

    @Override
    public ResponseEntity<List<CarEntity>> carsGet() {
        return ResponseEntity.ok(cars);
    }

    @Override
    public ResponseEntity<Void> carsIdDelete(@PathVariable("id") Integer id) {
        if (cars.removeIf(medicine -> id.equals(medicine.getId()))) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<CarEntity> medicinesIdGet(@PathVariable("id") Integer id) {
        return cars.stream()
                .filter(medicine -> id.equals(medicine.getId()))
                .map(ResponseEntity::ok)
                .findFirst()
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<CarEntity> carsIdPatch(@PathVariable("id") Integer id, @RequestBody CarEntity body) {
        Optional<CarEntity> carOptional = cars.stream()
                .filter(car -> id.equals(car.getId()))
                .findFirst();
        if (!carOptional.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        CarEntity car = carOptional.get();
        if (body.getName() != null) {
            car.setName(body.getName());
        }
        if (body.getPrice() != null) {
            car.setPrice(body.getPrice());
        }
        if (body.getType() != null) {
            car.setType(body.getType());
        }
        return ResponseEntity.ok(car);
    }

    @Override
    public ResponseEntity<CarEntity> carsIdPut(@PathVariable("id") Integer id, @RequestBody CarEntity body) {
        CarEntity car = cars.stream().filter(m -> id.equals(m.getId())).findFirst().orElse(null);
        boolean created = false;
        if (car == null) {
            car = new CarEntity();
            car.setId(idCounter++);
            created = true;
        }
        car.setName(body.getName());
        car.setPrice(body.getPrice());
        car.setType(body.getType());
        return new ResponseEntity<>(car, created ? HttpStatus.CREATED : HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CarEntity> medicinesPost(@RequestBody CarEntity body) {
        body.setId(idCounter++);
        cars.add(body);
        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }
}
