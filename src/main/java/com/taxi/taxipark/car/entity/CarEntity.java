package com.taxi.taxipark.car.entity;

import com.taxi.taxipark.car.table.CarTable;


import lombok.Data;

import javax.persistence.*;

/**
 * Created by fours on 06.06.2018.
 */
@Data
@Entity
@Table(name = CarTable.TABLE_NAME)
public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = CarTable.NAME, nullable = false, unique = true, length = 45)
    private String name;

    @Column(name = CarTable.MARK, nullable = false, length = 60)
    private String mark;

    @Column(name = CarTable.TYPE_, nullable = false)
    private String type_;


    @Column(name = CarTable.COST, nullable = false, length = 60)
    private int cost;

}
