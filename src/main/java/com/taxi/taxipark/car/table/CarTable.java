package com.taxi.taxipark.car.table;

/**
 * Created by fours on 06.06.2018.
 */
public class CarTable {
    private CarTable() {}

    public static final String TABLE_NAME = "cars";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String MARK = "mark";
    public static final String TYPE_ = "type_";
    public static final String COST = "cost";

}
