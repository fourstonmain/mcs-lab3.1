package com.taxi.taxipark.service_client.data;

/**
 * Created by fours on 05.06.2018.
 */
import com.taxi.taxipark.service_client.data.UserRole;
import com.taxi.taxipark.service_client.data.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
//@AllArgsConstructor
public class User {
    public Long id;
    public String login;
    public UserRole group;
    public String email;
    public String name;

    public User(Long id, String login, UserRole group, String email, String name) {
    }

    public User toDto() {
        return new User(id, login, group, email, name);
    }

    public String getLogin() {
        return login;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public UserRole getGroup() {
        return group;
    }

    public void setGroup(UserRole group) {
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}