package com.taxi.taxipark.user.repository;

/**
 * Created by fours on 05.06.2018.
 */
import org.springframework.data.repository.CrudRepository;
import com.taxi.taxipark.user.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByLogin(String login);
    UserEntity findOne(long id);

}
