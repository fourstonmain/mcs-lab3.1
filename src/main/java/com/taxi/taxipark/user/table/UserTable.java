package com.taxi.taxipark.user.table;

/**
 * Created by fours on 05.06.2018.
 */
public class UserTable {
    private UserTable() {}

    public static final String TABLE_NAME = "users";

    public static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String GROUP = "group";
    public static final String EMAIL = "email";
}
